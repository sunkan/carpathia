<?php
namespace Carpathia\Template;

interface ICallable
{
    public function callHelper($method, $args);
}
