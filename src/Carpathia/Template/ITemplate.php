<?php
namespace Carpathia\Template;

interface ITemplate {
    public function render();
    public function assign($key, $value=null);
}
