<?php

namespace Carpathia\Template;

use Carpathia\Template\Engine;

use Aura\Di\Container as Di;

use RuntimeException;
use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * Adds Template as a shared service.
 *
 * @author Kevin Herrera <kevin@herrera.io>
 */
class TemplateServiceProvider implements ServiceProviderInterface
{
    protected $di;

    public function __construct(Di $di) {
        $this->di = $di;
    }

    // @codeCoverageIgnoreStart
    /**
     * @override
     */
    public function boot(Application $app)
    {
    }
    // @codeCoverageIgnoreEnd

    /**
     * @override
     */
    public function register(Application $app)
    {
        $di = $this->di;
        $app['template.engine'] = $app->share(function() use ($app, $di) {
            $engine = $di->newInstance('Carpathia\Template\Extended');
            return $engine;
        });
    }
}