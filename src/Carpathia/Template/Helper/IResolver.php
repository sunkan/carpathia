<?php 

namespace Carpathia\Template\Helper;

use Carpathia\Template\ITemplate;

interface IResolver {
    public function resolve($method, ITemplate $template);
}