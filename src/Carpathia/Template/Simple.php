<?php

namespace Carpathia\Template;

class Simple implements ITemplate, ICallable {
    protected $_helperResolver = array();

    public function addResolver(Helper\IResolver $resolver) {
        $this->_helperResolver[] = $resolver;
    }
    public function setResolvers(array $resolvers) {
        $this->_helperResolver = [];
        foreach ($resolvers as $resolver) {
            if ($resolver instanceOf Helper\IResolver) {
                $this->addResolver($resolver);
            }
        }
    }
    public function callHelper($method, $args) {
        $helper = false;
        foreach ($this->_helperResolver as $resolver) {
            $helper = $resolver->resolve($method, $this);
            if ($helper) {
                break;
            }
        }
        if (!$helper) {
            return false;
        }
        if ($helper instanceOf Helper\IHelper) {
            return $helper->run($args);
        }
        return call_user_func_array(
            $helper,
            $args
        );
    }
    public function escape($value){
        return htmlspecialchars(
            $value,
            ENT_QUOTES,
            'UTF-8'
        );
    }
    public function __call($method,  $args) {
        return $this->callHelper($method, $args);
    }
    public function __set($var,  $value) {
        if ($var[0] != '_') {
            $this->$var = $value;
        }
    }
    public function assign($spec, $value = null) {
        if (\is_string($spec)) {
            if ($key[0] != "_") {
                $this->$spec = $value;
                return true;
            }
        }
        if (\is_array($spec)) {
            foreach ($spec as $key => $val) {
                if ($key[0] != "_") {
                   $this->$key = $val;
                }
            }
            return true;
        } 
        if (\is_object($spec)) {
            foreach (\get_object_vars($spec) as $key => $val) {
                if ($key[0] != "_") {
                    $this->$key = $val;
                }
            }
            return true;
        }
        return false;
    }
    public function render($file=null) {
        try {
            ob_start();
            require $file;

            $data = ob_get_clean();
            return $data;
        } catch (\Exception $e) {
        }
    }
}
