<?php

namespace Carpathia\MongoDb;

class Query
{
    /**
     * @var array
     */
    private $query = [];

    /**
     * @var array
     */
    private $ignore = [];

    /**
     * @param string $field
     * @param mixed $match
     * @return Carpathia\MongoDb\Query
     */
    public function addAnd($field, $match)
    {
        if (!isset($this->query['$and'])) {
            $this->query['$and'] = [];
        }
        $this->query['$and'][] = [$field => $match];
        return $this;
    }
    /**
     * @param string $field
     * @param array $in
     * @return Carpathia\MongoDb\Query
     */
    public function addIn($field, array $in)
    {
        return $this->add($field, [
            '$in' => $in,
        ]);
    }
    /**
     * @param string $field
     * @param mixed $match
     * @return Carpathia\MongoDb\Query
     */
    public function addOr($field, $match)
    {
        if (!isset($this->query['$or'])) {
            $this->query['$or'] = [];
        }
        $this->query['$or'][] = [$field => $match];
        $this->ignore[$field] = true;
        return $this;
    }
    /**
     * @param string $field
     * @param mixed $match
     * @return Carpathia\MongoDb\Query
     */
    public function add($field, $match)
    {
        if (isset($this->query[$field])) {
            $this->addAnd($field, $this->query[$field]);
            $this->addAnd($field, $match);
            $this->ignore[$field] = true;
        } else {
            $this->query[$field] = $match;
        }
        return $this;
    }
    /**
     * @return array
     */
    public function getQuery()
    {
        $query = $this->query;
        foreach ($this->ignore as $key => $value) {
            unset($query[$key]);
        }
        return $query;
    }
}
