<?php

namespace Carpathia\MongoDb;

use MongoClient;

class Client  extends MongoClient{

    public function getQuery() {
        return new Query();
    }
    public function find($query = array(), array $fields = array()) {
        if (!is_array($query) && $query instanceOf Query) {
            $query = $query->getQuery();
        }
        return parent::find($query, $fields);
    }
}