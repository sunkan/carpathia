<?php
namespace Carpathia\Db;

interface IConnection
{
    public function getProfiler();
    public function setProfiler(Profiler\IProfiler $profiler);

    public function prepare($stmt, $options=array());

    public function quoteName($name);

    public function isConnected();
    public function connect();
}
