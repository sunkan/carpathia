<?php
namespace Carpathia\Db;
use Aura\Signal\Manager as SignalManager;

class Connection implements IConnection
{
    protected $_nameClosing = '';
    protected $_nameOpening = '';

    /**
     *
     * @var PDO
     */
    protected $_pdo = null;

    protected $signal = null;

    /**
     *
     * @var Profiler\IProfiler
     */
    protected $profiler = null;

    protected $_config = array();

    public function __construct($config, $signalManager) {
        $this->_config = $config;
        $this->profiler = new Profiler\None();
        $this->signal = $signal;
    }

    public function getSignalManager() {
        return $this->signal;
    }

    /**
     *
     * @return Profiler\IProfiler
     */
    public function getProfiler() {
        return $this->profiler;
    }

    /**
     *
     * @param IProfiler $profiler
     * @return Connection
     */
    public function setProfiler(Profiler\IProfiler $profiler) {
        $this->profiler = $profiler;

        return $this;
    }

    /**
     *
     * @param string $type
     * @param string $value
     * @return string
     */
    public function _quote($type, $value) {
        if (in_array($type, array('name','field')))
        {
            return $this->quoteName($value);
        }
        return $this->_pdo->quote($value);
    }

    /**
     *
     * @return bool
     */
    public function isConnected() {
        try {
            if ( $this->_pdo === null ) {
                return false;
            }
            $this->_pdo->query('SELECT 1');
            return true;
        }
        catch ( PDOException $e ) {
            $this->connect();  // Don't catch exception here, so that
                               // re-connect fail will throw exception
        }

        return ($this->_pdo instanceof \PDO);
    }

    /**
     *
     * @return bool
     */
    public function connect($force=false) {
        if (($this->_pdo instanceof \PDO) && $force===false) {
            return true;
        }
        $config = $this->_config;

        $this->_pdo = new \PDO($config['dns'],
                               $config['username'],
                               $config['password'],
                               $config['options']);

        $flag = ($this->_pdo instanceof \PDO);
        if ($flag)
        {
            $this->_pdo->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );

            $this->_nameOpening = $this->_nameClosing = '`';
        }
        $params = array(
            'pdo'=>$this->_pdo,
            'flag'=>$flag
        );

        return $flag;
    }

    /**
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name,  $arguments) {
        if (!$this->isConnected())
        {
            $this->connect();
        }

        if(\method_exists($this->_pdo, $name))
        {
            return \call_user_func_array(array($this->_pdo, $name), $arguments);
        }

        return false;
    }

    /**
     *
     * @param string $sql
     * @param array $options
     * @return \PDOStatement
     */
    public function prepare($sql, $options=array()) {
        $sql = (string)$sql;
        if (!$this->isConnected()) {
            $this->connect();
        }
        if (!\key_exists(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, $options)) {
            $options[\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY] = true;
        }

        if (!\key_exists(\PDO::ATTR_STATEMENT_CLASS, $options)) {
            $options[\PDO::ATTR_STATEMENT_CLASS] = array('Carpathia\Db\Statement',
                array($this, $options)
            );
        }

        $stmt = $this->_pdo->prepare($sql, $options);

        return $stmt;
    }

    public function exec($statement,$options = array()) {
        if (!$this->isConnected()) {
            $this->connect();
        }
        $rs = $this->_pdo->exec($statement);

        return $rs;
    }

    /**
     *
     * @param string $name
     * @return string
     */
    public function quoteName($name)
    {
        $names = array();
        foreach (explode(".", $name) as $name)
        {
            $q = str_replace($this->_nameClosing, $this->_nameClosing.$this->_nameClosing, $name);
            $names[] = $this->_nameOpening . $q . $this->_nameClosing;
        }
        return implode(".", $names);
    }


}
