<?php
namespace Carpathia\Db;

class Statement extends \PDOStatement
{
    protected $_profiler = null;
    protected $_connection = null;
    protected $_options = array();
    protected $_id = null;
    
    protected function __construct($connection=null, $options=array())
    {
        parent::setFetchMode(\PDO::FETCH_OBJ);
        $this->_connection = $connection;
        $this->_options = $options;
        $this->_profiler = $connection->getProfiler();
    }
    public function lastInsertId()
    {
        if ($this->_id === null)
        {
            return $this->_id = $this->_connection->lastInsertId();
        }
        return $this->_id;
    }
    public function execute($params=array(),$options=array())
    {
        if ($this->_profiler->isEnabled())
        {
            $this->_profiler->queryStart($this->queryString,$params);
        }
        $rs = parent::execute($params);
        if (substr ( strtolower($this->queryString), 0, 6 )=='insert')
        {
            $this->_id = null;
            $this->lastInsertId();
        }
        if ($this->_profiler->isEnabled())
        {
            $this->_profiler->queryEnd();
        }

        return $rs;
    }
}
