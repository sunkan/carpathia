<?php

namespace Carpathia;

use Aura\Di\Container;
use Aura\Di\Factory;

class Di extends Container {
    protected $dir = '';
    protected $configs = [];
    public function __construct($dir) {
        $this->dir = $dir;
        parent::__construct(new Factory);
    }
    public function load($file) {
        $di = $this;
        include($this->dir.$file);
        return $this;
    }
    public function addConfig($class) {
        $config = $this->newInstance($class);
        $config->define($this);
        $this->configs[] = $config;
    }
}
