<?php

namespace Carpathia\Http;

use Aura\Http;

class Response extends \Aura\Http\Response {
    public function __construct(){
        parent::__construct(new Http\Headers(),new Http\Cookies());
    }
    public function setFormat($format) {
        $this->headers->set('Content-type','application/json');
    }
    public function setSignature($signature, $timestamp, $key) {
        $this->headers->set('X-'.$key.'-Timestamp',$timestamp);
        $this->headers->set('X-'.$key.'-Signature',$signature);
    }
    public function setJson(array $data) {
        $this->setContent(json_encode($data));
        $this->setFormat('json');
        return $this;
    }
    public function sendJson(array $data) {
        $this->setContent(json_encode($data));
        $this->setFormat('json');
        $this->send();
    }
}