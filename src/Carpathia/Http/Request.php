<?php
namespace Carpathia\Http;

class Request {

    /**
     * 
     * An array of http user-agents used in matching 
     * mobile browsers and crawlers
     *
     * @see isMobile()
     * @see isCrawler()
     * 
     * @var array
     * 
     */
    protected $agents = array(
        'mobile'=>array(
            'Android',
            'BlackBerry',
            'Blazer',
            'Brew',
            'IEMobile',
            'iPad',
            'iPhone',
            'iPod',
            'KDDI',
            'Kindle',
            'Maemo',
            'MOT-', // Motorola Internet Browser
            'Nokia',
            'SymbianOS',
            'UP.Browser', // Openwave Mobile Browser
            'UP.Link', 
            'Opera Mobi',
            'Opera Mini',        
            'webOS', // Palm devices
            'Playstation',
            'PS2',
            'Windows CE',
            'Polaris',
            'SEMC',
            'NetFront',
            'Fennec'
        ),
        'crawler'=>array(
            'Ask',
            'Baidu',
            'Google',        
            'AdsBot',
            'gsa-crawler',
            'adidxbot', 
            'librabot',
            'llssbot',
            'bingbot',
            'Danger hiptop',
            'MSMOBOT',
            'MSNBot',
            'MSR-ISRCCrawler',
            'MSRBOT',
            'Vancouver',
            'Y!J',
            'Yahoo',       
            'mp3Spider',
            'Mp3Bot',
            'Scooter',
            'slurp',
            'Y!OASIS',
            'YRL_ODP_CRAWLER',
            'Yandex',
            'Fast',
            'Lycos',
            'heritrix',
            'ia_archiver',
            'InternetArchive',
            'archive.org_bot',
            'Nutch',
            'WordPress',
            'Wget'
        )
    );
    
    /**
     * 
     * A property to hold previous calls to isMobile() 
     * so you don't have to loop through $this->agents['mobile'] again.
     * 
     * @var mixed
     * 
     */
    protected $is_mobile;
    
    /**
     * 
     * A property to hold previous calls to isCrawler() 
     * so you don't have to loop through $this->agents['crawler'] again.
     * 
     * @var mixed 
     * 
     */
    protected $is_crawler;
    public function filter($value, $filter,$options=null) {
        return \filter_var($value,$filter,$options);
    }
    public function get($key, $default=null, $filter=null,$options=null) {
        if ($filter) {
            $value = \filter_input(\INPUT_GET,$key,$filter,$options);
        }
        return $value === null?$default:$value;
    }
    public function post($value, $default, $filter) {

    }
    public function getParam($value, $default){

    }

    public function getUrl() {

    }
    public function getBaseUrl() {

    }
    public function setBaseUrl($uri) {

    }

    public function getScriptUrl() {
        if ($this->_scriptUrl===null) {
            $scriptName=basename($_SERVER['SCRIPT_FILENAME']);
            if (basename($_SERVER['SCRIPT_NAME'])===$scriptName) {
                $this->_scriptUrl=$_SERVER['SCRIPT_NAME'];
            } else if (basename($_SERVER['PHP_SELF'])===$scriptName) {
                $this->_scriptUrl=$_SERVER['PHP_SELF'];
            } else if (($pos=strpos($_SERVER['PHP_SELF'],'/'.$scriptName))!==false) {
                $this->_scriptUrl=substr($_SERVER['SCRIPT_NAME'],0,$pos).'/'.$scriptName;
            } else if (isset($_SERVER['DOCUMENT_ROOT']) && strpos($_SERVER['SCRIPT_FILENAME'],$_SERVER['DOCUMENT_ROOT'])===0) {
                $this->_scriptUrl=str_replace('\\','/',str_replace($_SERVER['DOCUMENT_ROOT'],'',$_SERVER['SCRIPT_FILENAME']));
            } else {
                throw new \Exception('Unable to determine the entry script URL.');
            }
        }
        return $this->_scriptUrl;
    }

    public function getPathInfo() {

    }

    public function getRequestUri() {
        return preg_replace('#\?.*$#D', '', $_SERVER['REQUEST_URI']);
    }

    public function getQueryString() {
        return isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'';
    }

    public function getIsSecureConnection() {
        return isset($_SERVER['HTTPS']) && !strcasecmp($_SERVER['HTTPS'],'on');
    }
    public function getRequestType() {
        return strtoupper(isset($_SERVER['REQUEST_METHOD'])?$_SERVER['REQUEST_METHOD']:'GET');
    }

    public function isPost() {
        return (bool)(strtolower($_SERVER['REQUEST_METHOD'])=='post');
    }
    public function isGet() {
        return (bool)(strtolower($_SERVER['REQUEST_METHOD'])=='get');
    }
    public function isAjax() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
                (\strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'));
    }

    public function getServerName() {
        return $_SERVER['SERVER_NAME'];
    }

    public function getServerPort() {
        return $_SERVER['SERVER_PORT'];
    }

    public function getUrlReferrer() {
        return isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:null;
    }

    public function getUserAgent() {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    public function getUserHostAddress() {
        // check for shared internet/ISP IP
        if (!empty($_SERVER['HTTP_CLIENT_IP']) && $this->_validateIp($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }

        // check for IPs passing through proxies
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // check if multiple ips exist in var
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
                $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                foreach ($iplist as $ip) {
                    if ($this->_validateIp($ip)) {
                        return $ip;
                    }
                }
            }
            elseif ($this->_validateIp($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED']) && $this->_validateIp($_SERVER['HTTP_X_FORWARDED'])) {
            return $_SERVER['HTTP_X_FORWARDED'];
        }
        if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && $this->_validateIp($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
            return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        }
        if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && $this->_validateIp($_SERVER['HTTP_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_FORWARDED_FOR'];
        }
        if (!empty($_SERVER['HTTP_FORWARDED']) && $this->_validateIp($_SERVER['HTTP_FORWARDED'])) {
            return $_SERVER['HTTP_FORWARDED'];
        }
        if (!empty($_SERVER['X-REAL-IP']) && $this->_validateIp($_SERVER['HTTP_FORWARDED'])) {
            return $_SERVER['HTTP_FORWARDED'];
        }

        // return unreliable ip since all else failed
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
    * Ensures an ip address is both a valid IP and does not fall within
    * a private network range.
    */
    private function _validateIp($ip) {
        if (strtolower($ip) === 'unknown') {
            return false;
        }

        // generate ipv4 network address
        $ip = ip2long($ip);

        // if the ip is set and not equivalent to 255.255.255.255
        if ($ip !== false && $ip !== -1) {
            // make sure to get unsigned long representation of ip
            // due to discrepancies between 32 and 64 bit OSes and
            // signed numbers (ints default to signed in PHP)
            $ip = sprintf('%u', $ip);
            // do private network range checking
            if ($ip >= 0 && $ip <= 50331647) return false;
            if ($ip >= 167772160 && $ip <= 184549375) return false;
            if ($ip >= 2130706432 && $ip <= 2147483647) return false;
            if ($ip >= 2851995648 && $ip <= 2852061183) return false;
            if ($ip >= 2886729728 && $ip <= 2887778303) return false;
            if ($ip >= 3221225984 && $ip <= 3221226239) return false;
            if ($ip >= 3232235520 && $ip <= 3232301055) return false;
            if ($ip >= 4294967040) return false;
        }
        return true;
    }


    /**
    * @return string user host name, null if cannot be determined
    */
    public function getUserHost() {
        return isset($_SERVER['REMOTE_HOST'])?$_SERVER['REMOTE_HOST']:null;
    }

    /**
    * @return string entry script file path (processed w/ realpath())
    */
    public function getScriptFile() {
        if ($this->_scriptFile!==null) {
            return $this->_scriptFile;
        } else {
            return $this->_scriptFile=realpath($_SERVER['SCRIPT_FILENAME']);
        }
    }

    public function getBrowser($userAgent=null) {
        return \get_browser($userAgent,true);
    }

    /**
    * @return string user browser accept types
    */
    public function getAcceptTypes() {
        return $_SERVER['HTTP_ACCEPT'];
    }

    public function getCookies(){
        return $_COOKIE;
    }

    public function getPreferredLanguage() {
        if ($this->_preferredLanguage===null) {
            if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) &&
                    ($n=preg_match_all('/([\w\-_]+)\s*(;\s*q\s*=\s*(\d*\.\d*))?/',$_SERVER['HTTP_ACCEPT_LANGUAGE'],$matches))>0)
            {
                $languages=array();
                for($i=0;$i<$n;++$i) {
                    $languages[$matches[1][$i]]=empty($matches[3][$i]) ? 1.0 : floatval($matches[3][$i]);
                }
                arsort($languages);
                foreach($languages as $language=>$pref) {
                    return $this->_preferredLanguage=$language;
                }
            }
            return $this->_preferredLanguage=false;
        }
        return $this->_preferredLanguage;
    }
    public function getCsrfToken($url = null) {
        return $this->createCsrfCookie($url);
    }
    protected function createCsrfCookie($url = null) {
        if ($url === null) {
            $url = $this->getRequestUri();
        }
        $key = 'Carpathia_Http_Request::'.$url.'::csrf_tokens';
        if (!isset($_SESSION[$key]))
        {
            $_SESSION[$key] = array();
        }
        return $_SESSION[$key][] = md5(uniqid(rand(), true));
    }

    public function validateCsrfToken($token, $url=null) {
        if ($url === null) {
            $url = $this->getRequestUri();
        }
        $key = 'Carpathia_Http_Request::'.$url.'::csrf_tokens';
        $tokens = isset($_SESSION[$key])?$_SESSION[$key]:array();

        if (!in_array($token, $tokens)) {
            return false;
        }

        $tokens = array_diff($tokens, array($token));
        $_SESSION[$key] = $tokens;
        return true;
    }
    /**
     *  
     * Is this a mobile device? 
     * 
     * @return mixed False if not mobile, or the matched pattern if it is.
     * 
     */
    public function isMobile()
    {
        // have we found a mobile agent previously?
        if ($this->is_mobile !== null) {
            // yes, return it
            return $this->is_mobile;
        }
        
        // by default, not mobile
        $this->is_mobile = false;
        
        // what is the actual user-agent string?
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        
        // look for mobile agents
        foreach ($this->agents['mobile'] as $agent) {
            $find = preg_quote($agent);
            $match = preg_match("/$find/i", $user_agent); // case-insensitive
            if ($match) {
                $this->is_mobile = $agent;
                break;
            }
        }
        
        // done!
        return $this->is_mobile;
    }
    
    /**
     *  
     * Is this a crawler/bot device? 
     * 
     * @return mixed False if not a crawler, or the matched pattern if it is.
     * 
     */
    public function isCrawler()
    {
        // have we found a crawler agent previously?
        if ($this->is_crawler !== null) {
            // yes, return it
            return $this->is_crawler;
        }
        
        // by default, not crawler
        $this->is_crawler = false;
        
        // what is the actual user-agent string?
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        
        // look for crawler agents
        foreach ($this->agents['crawler'] as $agent) {
            $find = preg_quote($agent);
            $match = preg_match("/$find/i", $user_agent); // case-insensitive
            if ($match) {
                $this->is_crawler = $agent;
                break;
            }
        }
        
        // done!
        return $this->is_crawler;
    }
}

