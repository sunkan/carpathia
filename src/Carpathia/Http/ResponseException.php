<?php
namespace Carpathia\Http;

class ResponseException extends \Exception {
    private $status;
    protected $jsend_status = 'error';
    protected $data = [];
    public function __construct($msg,$code=0,$http_code=null, $data=[]) {
        $this->data = $data;
        $this->status = $http_code>0?$http_code:$code;
        parent::__construct($msg,$code);
    }
    public function getJSendStatus() {
        return $this->jsend_status;
    }
    public function getHttpStatus() {
        return $this->status;
    }
    public function setData(array $data) {
        $this->data = $data;
    }
    public function getData() {
        return $this->data;
    }
}
