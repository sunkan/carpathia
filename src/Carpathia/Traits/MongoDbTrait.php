<?php

namespace Carpathia\Traits;

use MongoClient;

trait MongoDbTrait
{
    /**
     * @var MongoClient
     */
    protected $MongoClient;

    /**
     * @param MongoClient $conn
     */
    public function setMongoClient(MongoClient $conn)
    {
        $this->MongoClient = $conn;
    }

    /**
     * @param  string $db
     * @return MongoCollection
     */
    public function getMongoDb($db)
    {
        return $this->MongoClient->$db;
    }

    /**
     * @return Carpathia\MongoDb\Client
     */
    public function getMongoClient()
    {
        return $this->MongoClient;
    }
}
