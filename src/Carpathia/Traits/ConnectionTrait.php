<?php

namespace Carpathia\Traits;

trait ConnectionTrait
{
    /**
     * @var PDO
     */
    protected static $connection;

    /**
     * @var array
     */
    protected static $stmts = [];

    /**
     * @var PDO
     */
    protected $local_connection;

    /**
     * @var array
     */
    protected $local_stmts = [];

    /**
     * @param $conn
     */
    public static function setStaticConnection($conn)
    {
        self::$connection = $conn;
    }

    /**
     * @param $conn
     */
    public static function getStaticConnection($conn)
    {
        return self::$connection;
    }

    /**
     * @return PDO
     */
    public function getConnection()
    {
        return $this->local_connection ?: self::$connection;
    }

    /**
     * @param PDO $conn
     */
    public function setConnection($conn)
    {
        $this->local_connection = $conn;
    }

    /**
     * @param $sql
     * @return mixed
     */
    public function prepareStmt($sql)
    {
        $key = sha1($sql);
        if ($this->local_connection) {
            if (!isset($this->local_stmts[$key])) {
                $this->local_stmts[$key] = $this->local_connection->prepare($sql);
            }
            return $this->local_stmts[$key];
        }

        if (!isset(self::$stmts[$key])) {
            self::$stmts[$key] = self::$connection->prepare($sql);
        }
        return self::$stmts[$key];
    }
}
