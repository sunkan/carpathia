<?php

namespace Carpathia\Traits;

trait MemcachedTrait
{
    /**
     * @var Memcached
     */
    protected static $memcached;
    /**
     * @var Memcached
     */
    protected $local_memcached;

    /**
     * @param $mem
     */
    public static function setStaticMemcached($mem)
    {
        self::$memcached = $mem;
    }
    /**
     * @param $mem
     */
    public function setMemcached($mem)
    {
        $this->local_memcached = $mem;
    }

    /**
     * @return Memcached
     */
    public function getMemcached()
    {
        return $this->local_memcached ?: self::$memcached;
    }

    /**
     * @return Memcached
     */
    public static function getStaticMemcached()
    {
        return self::$memcached;
    }
}
