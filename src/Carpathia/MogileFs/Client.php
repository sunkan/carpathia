<?php
/**
 * A minimalistic PHP mogilefs client.
 *
 * Copyright (c) 2009-2013 Andreas Sundqvist
 *
 * Distributed under the terms of the MIT License.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright  2013-2014 Andreas Sundqvist <andreas@sunkan.se>
 * @license    http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link       http://bitbucket.org/sunkan/Carpathia/MogileFs
 */
namespace Carpathia\MogileFs;

class Client {
    /**
     * Tracker success code
     */
    const SUCCESS = 'OK';
    /**
     * Tracker fail code
     */
    const ERROR   = 'ERR';

    private $request_timeout = 10;
    private $read_timeout = 10;
    private $trackers = [];

    public function __construct(array $trackers=array()) {
        $this->trackers = $trackers;
    }

    public function setTrackers(array $trackers) {
        $this->trackers = $trackers;
        return $this;
    }

    private function getConnection($trackers) {
        if($this->socket && is_resource($this->socket) && !feof($this->socket)){
            return $this->socket;
        }

        foreach($trackers as $host) {
            $parts = parse_url($host);
            if(!isset($parts['port'])) {
                $parts['port'] = 7001;
            }

            $errno = null;
            $errstr = null;
            $this->socket = fsockopen($parts['host'], $parts['port'], $errno, $errstr, $this->request_timeout);
            if($this->socket){
                break;
            }
        }

        if(!is_resource($this->socket) || feof($this->socket)) {
            throw new \RuntimeException(get_class($this) . "::connect failed to obtain connection");
        } else {
            return $this->socket;
        }
    }
    public function doRequest($cmd, $args=[]) {
        $params = '';
        if (count($args)) {
            foreach ($args as $key => $value) {
                $params .= '&'.urlencode($key).'='.urlencode($value);
            }
        }
        if (!$this->isConnected() && ! $this->connect()) {
            throw new \RuntimeException(get_class($this).'::doRequest failed to obtain connection');
        }
        $socket = $this->socket;

        $result = fwrite($socket, $cmd . $params . "\n");
        if($result === false) {
            throw new \UnexpectedValueException(get_class($this) . "::doRequest write failed");
        }
        $line = fgets($socket);
        if($line === false) {
            throw new \UnexpectedValueException(get_class($this) . "::doRequest read failed");
        }

        $words = explode(' ', $line);
        if($words[0] == self::SUCCESS) {
            parse_str(trim($words[1]), $result);
        } else {
            if(!isset($words[1])) {
                $words[1] = null;
            }
            switch($words[1]) {
                case 'unknown_key':
                    throw new UnknownKey(get_class($this) . "::doRequest unknown_key {$args['key']}");
                case 'empty_file':
                    throw new EmptyFile(get_class($this) . "::doRequest empty_file {$args['key']}");
                default:
                    throw new \Exception(get_class($this) . "::doRequest " . trim(urldecode($line)));
            }
        }
        return $result;
    }

    /**
     * bool MogileFs::connect(string $host, int $port, string $domain[, float $timeout])
     */
    public function connect($timeout=10) {
        $this->domain = $domain;
        $this->request_timeout = $timeout;
        $this->getConnection($this->trackers);
        return $this->isConnected();
    }

    /**
     * bool MogileFs::isConnection()
     */
    public function isConnected() {
        return $this->socket && is_resource($this->socket) && !feof($this->socket);
    }

    /**
     * bool MogileFs::close()
     */
    public function close() {
        if ($this->isConnected()) {
            return fclose($this->socket);
        }
        return true;
    }

    /**
     * array MogileFs::createClass(string $domain, string $class, string $mindevcount)
     * CREATE_CLASS domain=%s&class=%s&mindevcount=%d
     */
    public function createClass($domain, $class, $mindevcount) {
        if (!is_int($mindevcount)) {
            throw new InvalidArgumentException(get_class($this) . "::createClass mindevcount must be an integer");
        }
        return $this->_doRequest('CREATE_CLASS', [
            'domain'=>$domain,
            'class'=>$class,
            'mindevcount'=>$mindevcount
        ]);
    }

    /**
     * array MogileFs::updateClass(string $domain, string $class, string $mindevcount)
     * UPDATE_CLASS domain=%s&class=%s&mindevcount=%d&update=1
     */
    public function updateClass($domain, $class, $mindevcount) {
        if (!is_int($mindevcount)) {
            throw new InvalidArgumentException(get_class($this) . "::updateClass mindevcount must be an integer");
        }
        return $this->_doRequest('UPDATE_CLASS', [
            'domain'=>$domain,
            'class'=>$class,
            'mindevcount'=>$mindevcount,
            'update'=>1
        ]);
    }

    /**
     *
     * DELETE_CLASS domain=%s&class=%s
     */
    public function deleteClass($domain, $class){
        return $this->_doRequest('DELETE_CLASS', [
            'domain'=>$domain,
            'class'=>$class
        ]);
    }


    /**
     * void Mogilefs::setReadTimeout(float $readTimeout)
     */
    public function setReadTimeout($readTimeout) {
        if (is_int($readTimeout) || is_float($readTimeout)) {
            $this->read_timeout = $readTimeout;
            return $this;
        }
        throw new InvalidArgumentException("Read timeout must be an integer or float. readTimeout was:".$readTimeout);
    }

    /**
     * float MogileFs::getReadTimeout()
     */
    public function getReadTimeout() {
        return $this->read_timeout;
    }

}