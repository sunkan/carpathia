<?php

namespace Carpathia\MogileFs\Commands;

use Carpathia\MogileFs\Client;

abstract class ACommand {
  protected $client;
  public function __construct(Client $client) {
    $this->client = $client;
  }
}