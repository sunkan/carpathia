<?php

namespace Carpathia\MogileFs\Commands;

use Carpathia\MogileFs\Client;

class Utils extends ACommand {

    /**
     * array MogileFs::listKeys(string $prefix, string $after, integer $limit)
     */
    public function listKeys($domain, $prefix, $after, $limit) {
        $result = $this->client->doRequest('LIST_KEYS', [
            'domain'=>$domain,
            'prefix'=>$prefix,
            'after'=>$after,
            'limit'=>(int)$limit
        ]);

        return $result;
    }

    /**
     * bool MogileFs::listFids(integer $from, integer $to)
     */
    public function listFids($from, $to, $domain) {
        if (!is_int($from)) {
            throw new \InvalidArgumentException(get_class($this) . "::listFids from must be an integer");
        }
        if (!is_int($to)) {
            throw new \InvalidArgumentException(get_class($this) . "::listFids to must be an integer");
        }
        $result = $this->client->doRequest('LIST_FIDS', [
            'domain'=>$domain,
            'from'=>$from,
            'to'=>$to
        ]);
        return $result;
    }

    /**
     * bool MogileFs::sleep(integer $duration)
     * SLEEP domain=%s&duration=%d
     */
    public function sleep($duration) {
        $this->client->doRequest('SLEEP', [
            'duration'=>$duration
        ]);
        return true;
    }

    /**
     * array MogileFs::stats(integer $all)
     *   STATS domain=%s&all=%s
     */
    public function stats($all=1) {
        if (!is_int($all)) {
            throw new \InvalidArgumentException(get_class($this) . "::stats all must be an integer");
        }
        if ($all>1) {
            $all = 1;
        }
        return $this->client->doRequest('STATS', [
            'all'=>$all
        ]);
    }

    /**
     * bool MogileFs::replicate()
     * REPLICATE_NOW domain=%s
     */
    public function replicate() {
        return $this->client->doRequest('REPLICATE_NOW');
    }


    /**
     * bool MogileFs::checker(string $status ("on" or "off"), string $level)
     * CHECKER domain=%s&disable=%s&level=%s
     */
    public function checker($status, $level) {
        return $this->client->doRequest('CHECKER', [
            'disable'=>$status,
            'level'=>$level
        ]);
    }
}