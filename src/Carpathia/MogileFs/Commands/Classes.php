<?php

namespace Carpathia\MogileFs\Commands;

use Carpathia\MogileFs\Client;

class Classes extends ACommand {
    private $domain = '';
    public function __construct(Client $client, $domain) {
        parent::__construct($client);
        $this->domain = $domain;
    }
    /**
     * array MogileFs::createClass(string $domain, string $class, string $mindevcount)
     * CREATE_CLASS domain=%s&class=%s&mindevcount=%d
     */
    public function create($class, $mindevcount) {
        if (!is_int($mindevcount)) {
            throw new InvalidArgumentException(get_class($this) . "::createClass mindevcount must be an integer");
        }
        return $this->_doRequest('CREATE_CLASS', [
            'domain'=>$this->domain,
            'class'=>$class,
            'mindevcount'=>$mindevcount
        ]);
    }

    /**
     * array MogileFs::updateClass(string $domain, string $class, string $mindevcount)
     * UPDATE_CLASS domain=%s&class=%s&mindevcount=%d&update=1
     */
    public function update($class, $mindevcount) {
        if (!is_int($mindevcount)) {
            throw new InvalidArgumentException(get_class($this) . "::updateClass mindevcount must be an integer");
        }
        return $this->_doRequest('UPDATE_CLASS', [
            'domain'=>$this->domain,
            'class'=>$class,
            'mindevcount'=>$mindevcount,
            'update'=>1
        ]);
    }

    /**
     *
     * DELETE_CLASS domain=%s&class=%s
     */
    public function remove($class){
        return $this->_doRequest('DELETE_CLASS', [
            'domain'=>$this->domain,
            'class'=>$class
        ]);
    }

}