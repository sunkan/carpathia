<?php

namespace Carpathia\MogileFs\Commands;

use Carpathia\MogileFs\Client;

class File extends ACommand {
    private $domain = '';
    public function __construct(Client $client, $domain) {
        parent::__construct($client);
        $this->domain = $domain;
    }
    public function upload($file, $key, $class, $use_file=true) {
        if($key === null) {
            throw new \InvalidArgumentException(get_class($this) . "::upload key cannot be null");
        }
        if ($use_file) {
            $fh = fopen($file, 'r');
            if (!$fh) {
                throw new \RuntimeException(get_class($this) . "::upload failed to open file");
            }
            $length = filesize($file);
        } else {
            $fh = fopen('php://memory', 'rw');
            if($fh === false) {
                throw new \RuntimeException(get_class($this) . "::upload failed to open memory stream");
            }
            $length = fwrite($fh, $file);
            rewind($fh);
        }

        //CREATE_OPEN domain=%s&key=%s&class=%s&multi_dest=%d
        $location = $this->client->doRequest('CREATE_OPEN', [
            'domain'=>$this->domain,
            'key'=>$key,
            'class'=>$class
        ]);
        $uri = $location['path'];
        $parts = parse_url($uri);
        $host = $parts['host'];
        $port = $parts['port'];
        $path = $parts['path'];

        $ch = curl_init();
        #curl_setopt($ch, CURLOPT_VERBOSE, ($this->debug > 0 ? 1 : 0));
        curl_setopt($ch, CURLOPT_INFILE, $fh);
        curl_setopt($ch, CURLOPT_INFILESIZE, $length);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->request_timeout);
        curl_setopt($ch, CURLOPT_PUT, 1);
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Expect: ']);
        $response = curl_exec($ch);
        fclose($fh);
        if($response === false) {
            $error=curl_error($ch);
            curl_close($ch);
            throw new \RuntimeException(get_class($this) . "::upload $error");
        }
        curl_close($ch);
        $this->client->doRequest('CREATE_CLOSE', [
            'key'   => $key,
            'class' => $class,
            'domain'=> $this->domain,
            'devid' => $location['devid'],
            'fid'   => $location['fid'],
            'path'  => urldecode($uri),
        ]);

        return true;
    }
    public function fileInfo($key) {
        if($key === null) {
            throw new \InvalidArgumentException(get_class($this) . "::fileInfo key cannot be null");
        }
        $result = $this->client->doRequest('FILE_INFO', [
            'domain'=>$this->domain,
            'key'=>$key
        ]);

        return $result;
    }
    public function get($key, $pathcount=2) {
        if($key === null) {
            throw new \InvalidArgumentException(get_class($this) . "::get key cannot be null");
        }

        $result = $this->client->doRequest('GET_PATHS', [
            'domain'=>$this->domain,
            'key' => $key,
            'pathcount'=>$pathcount
        ]);

        return $result;
    }
    public function rename($from_key, $to_key) {
        if($from_key === null) {
            throw new \InvalidArgumentException(get_class($this) . "::rename from_key cannot be null");
        }
        if($to_key === null) {
            throw new \InvalidArgumentException(get_class($this) . "::rename to_key cannot be null");
        }

        $this->client->doRequest('RENAME', [
            'domain'=>$this->domain,
            'from_key'=>$from_key,
            'to_key'=>$to_key
        ]);
        return true;
    }
    public function remove($key) {
        if($key === null) {
            throw new \InvalidArgumentException(get_class($this) . "::remove key cannot be null");
        }

        $result = $this->_doRequest('DELETE', [
            'domain'=>$this->domain,
            'key' => $key
        ]);

        return true;
    }

}