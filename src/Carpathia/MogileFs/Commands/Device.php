<?php

namespace Carpathia\MogileFs\Commands;

class Device extends ACommand {

    private $device_status = ['alive', 'dead', 'down', 'drain', 'readonly'];

    /**
     * array MogileFs::getDevices()
     */
    public function get() {
        return $this->client->doRequest('GET_DEVICES');
    }

    /**
     * array MogileFs::createDevice(string $devid, string $status)
     * CREATE_DEVICE domain=%s&status=%s&devid=%s
     */
    public function create($devId, $status) {
        if (!is_int($devId)) {
            throw new \InvalidArgumentException(get_class($this) . "::createDevice devId must be an integer");
        }
        $status = strtolower($status);
        if (!in_array($status, $this->device_status)) {
            throw new \InvalidArgumentException(get_class($this) . "::createDevice status must be one off this:".implode(', ', $this->device_status));
        }
        return $this->client->doRequest('CREATE_DEVICE', [
            'status'=>$status,
            'devid'=>$devId
        ]);
    }

    /**
     * bool MogileFs::setWeight(string $hostname, string $device, string $weight)
     * SET_WEIGHT domain=%s&host=%s&device=%s&weight=%s
     */
    public function setWeight($hostname, $device, $weight) {
        return $this->client->doRequest('SET_WEIGHT', [
            'host'=>$hostname,
            'device'=>$device,
            'weight'=>$weight
        ]);
    }

    /**
     * bool MogileFs::setState(string $hostname, string $device[, string $state = "alive"])
     * SET_STATE domain=%s&host=%s&device=%s&state=%s
     */
    public function setState($hostname, $devId, $status='alive') {
        if (!in_array($status, $this->device_status)) {
            throw new \InvalidArgumentException(get_class($this) . "::setState status must be one off: ".implode(', ', $this->device_status));
        }
        return $this->client->doRequest('SET_STATE', [
            'host'=>$hostname,
            'device'=>$devId,
            'status'=>$status
        ]);
    }
}