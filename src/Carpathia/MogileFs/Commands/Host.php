<?php

namespace Carpathia\MogileFs\Commands;

class Host extends ACommand {

    /**
     * array MogileFs::getHosts()
     * GET_HOSTS domain=%s
     */
    public function get() {
        return $this->client->doRequest('GET_HOSTS');
    }

    /**
     * array MogileFs::createHost(string domain, string host, string ip, int port)
     * CREATE_HOST domain=%s&host=%s&ip=%s&port=%s
     */
    public function create($host, $ip, $port) {
        return $this->client->doRequest('CREATE_HOST', [
            'host'=>$host,
            'ip'=>$ip,
            'port'=>$port
        ]);
    }

    /**
     * array MogileFs::update(string $hostname, string $ip, int $port[, string $state = "alive"])
     * UPDATE_HOST domain=%s&host=%s&ip=%s&port=%s&status=%s&update=1
     */
    public function update($hostname, $ip, $port, $status='alive') {
        if (!in_array($status, ['alive','dead','down'])) {
            throw new \InvalidArgumentException(get_class($this) . "::update status must be one off: alive, dead, down");
        }
        return $this->client->doRequest('UPDATE_HOST', [
            'host'=>$hostname,
            'ip'=>$ip,
            'port'=>$port,
            'status'=>$status,
            'update'=>1
        ]);
    }

    /**
     * bool MogileFs::deleteHost(string $hostname)
     * DELETE_HOST domain=%s&host=%s
     */
    public function remove($hostname) {
        return $this->client->doRequest('DELETE_HOST', [
            'host'=>$hostname
        ]);
    }

}