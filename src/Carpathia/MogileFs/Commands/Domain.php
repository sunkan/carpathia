<?php

namespace Carpathia\MogileFs\Commands;

class Domain extends ACommand {

    /**
     * array createDomain(string $domain)
     * CREATE_DOMAIN domain=%s
     */
    public function create($domain) {
        return $this->client->doRequest('CREATE_DOMAIN', [
            'domain'=>$domain
        ]);
    }

    /**
     * array deleteDomain(string $domain)
     */
    public function remove($domain) {
        return $this->client->doRequest('DELETE_DOMAIN', [
            'domain'=>$domain
        ]);
    }


    /**
     * array MogileFs::getDomains()
     */
    public function get() {
        $res = $this->client->doRequest('GET_DOMAINS');

        $domains = [];
        for($i=1; $i <= $res['domains']; $i++)  {
            $dom = 'domain'.$i;
            $classes = [];
            for($j=1; $j <= $res[$dom.'classes']; $j++) {
                $classes[$res[$dom.'class'.$j.'name']] = $res[$dom.'class'.$j.'mindevcount'];
            }
            $domains[] = [
                'name' => $res[$dom],
                'classes' => $classes
            ];
        }
        return $domains;
    }
}