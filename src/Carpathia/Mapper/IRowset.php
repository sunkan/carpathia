<?php
namespace Carpathia\Mapper;
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Sunkan
 */
interface IRowset extends \Carpathia\Pager\Collection\ICollection
{
    public function setMapper(AMapper $mapper);
    public function getMapper();
    public function getRow($pos);
    public function delete();
    public function save();
}
