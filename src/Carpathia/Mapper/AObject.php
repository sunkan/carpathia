<?php
namespace Carpathia\Mapper;

abstract class AObject
{
    protected $_mapper = null;
    protected $_data = array();
    protected $_sleepData = array();
    protected $_dirtyKeys = array();

    public function __sleep() {
        if (($this->_mapper instanceof AMapper)) {
            $this->_mapper = $this->_mapper->getMapperName();
        }
        $this->_sleepData = $this->_data['orginal'];
        return array('_mapper','_sleepData');
    }
    public function __wakeup() {
        $this->_data['dirty'] = $this->_data['orginal'] = $this->_sleepData;
    }

    public function __construct($data, $config=array())
    {
        $this->setData((array)$data);
        $this->_data['orginal'] = (array)$data;
        $this->_data['relations'] = array();
        if(isset ($config['mapper']))
        {
            $this->setMapper($config['mapper']);
        }
    }
    public function getMapper()
    {
        if (!($this->_mapper instanceof AMapper)) {
            $this->_mapper = $this->_loadMapper();
        }
        return $this->_mapper;
    }
    public function setMapper($mapper)
    {
        $this->_mapper = $mapper;
        if (count($this->_dirtyKeys)>0)
        {
            $this->_loadMapper()->register($this, 'dirty');
        }
    }
    protected function _loadMapper($name=false,$reload=false,$cache=false)
    {
        if ($reload) {
            if (($this->_mapper instanceof AMapper)) {
                $this->_mapper = $this->_mapper->getMapperName();
            }
        }
        if ($name===false)
        {
            $name = $this->_mapper;
        }
        if (is_string($name))
        {
             return AMapper::loadMapper($name,$cache);
        }
        return $name;
    }
    public function setId($id)
    {
        if (isset ($this->_data['orginal']['id']))
        {
            return false;
        }
        $this->_data['orginal']['id'] = $id;
        $this->_data['dirty']['id'] = $id;
    }
    public function getId()
    {
        return $this->_data['orginal']['id'];
    }
    public function getObjectKey()
    {
        return $this->getId();
    }

    public function setExtra($name,$value)
    {
        if (!isset($this->_data['extra'])) $this->_data['extra'] = array();
        $this->_data['extra'][$name] = $value;
    }
    public function getExtra($key)
    {
        return $this->_data['extra'][$key];
    }

    public function __isset($name)
    {
        if(isset ($this->_data['dirty'][$name]))
        {
            return true;
        }

        if (isset ($this->_relations[$name]))
        {
            return $this->getRelation($name)!==null;
        }

        return false;
    }

    public function  __set($name,  $value)
    {
        if($name == 'id')
            return false;


        $method = 'set'.ucfirst($name);
        if (method_exists($this, $method))
        {
            return $this->$method($value);
        }
        return $this->_set($name, $value);
    }
    protected function _set($name,  $value) {
        if ($this->_data['orginal'][$name]!==$value) {
            $this->_dirtyKeys[] = $name;
            $this->_data['dirty'][$name] = $value;

            if ($this->getId()!==null && $this->_mapper instanceof AMapper)
            {
                $this->_mapper->register($this,'dirty');
            }
        }
        return true;
    }
    public function __get($name)
    {
        $method = 'get'.ucfirst($name);
        if (method_exists($this, $method))
        {
            return $this->$method();
        }
        if(isset ($this->_data['dirty'][$name]))
        {
            if (!mb_detect_encoding($this->_data['dirty'][$name],'UTF-8',true)) {
                return utf8_encode($this->_data['dirty'][$name]);
            }
            return $this->_data['dirty'][$name];
        }

        if (isset ($this->_data['relations'][$name]))
        {
            return $this->_data['relations'][$name];
        }
        return $this->getRelation($name);
    }
    public function getRelation($name) {
        return $this->_data['relations'][$name] = $this->_loadMapper()->getRelation($name,$this);
    }
    public function __call($name, $args){
        $rs = preg_match('/^(get|count)([A-Za-z]*)/',$name,$m);
        if ($rs===1){
            $name = strtolower($m[2]);
            if ($m[1]=='get'){
                $extra = array(
                    'order'=>(isset($args[1])?$args[1]:'id DESC'),
                    'where'=>(isset($args[0]) && is_array($args[0])?$args[0]:array()),
                    'offset'=>(isset($args[2])?(int)$args[2]:0),
                    'limit'=>(isset($args[3])?(int)$args[3]:15)
                );
                $data = $this->_loadMapper()->getRelation($name,$this,$extra);
                $this->_data['relations'][$name] = $data;
                if ($data !== false){
                    return $data;
                }
            } elseif ($m[1]=='count') {
                $extra = array(
                    'method'=>'count',
                    'where'=>(isset($args[0]) && is_array($args[0])?$args[0]:array())
                );
                $data = $this->_loadMapper()->getRelation($name,$this,$extra);
                if ($data !== false){
                    return $data;
                }
            }
        }
        throw new \BadMethodCallException('No method by that name:'.$name);
    }
    public function delete()
    {
        return (bool)$this->_loadMapper()->delete($this);
    }
    public $isNew = false;
    public function save()
    {
        if ($this->getId()==null)
        {
            $id = $this->_loadMapper()->insert($this);
            $this->setId($id);
            if ($id) {
                $this->_data['orginal'] = $this->_data['dirty'];
            }
            return $id;
        }
        if (count($this->_dirtyKeys)>0)
        {
            if ((bool)$this->_loadMapper()->update($this)){
                $this->_data['orginal'] = $this->_data['dirty'];
                return true;
            }
            return false;
        }
        return true;
    }
    public function get($keys,$dataType='orginal')
    {
        if (!in_array($dataType,array('orginal','dirty')))
        {
            $dataType = 'orginal';
        }
        if (is_array($keys))
        {
            $return  = array();
            foreach ($keys as $key)
            {
                if (!mb_detect_encoding($this->_data[$dataType][$key],'UTF-8',true)) {
                    $return[$key] = utf8_encode($this->_data[$dataType][$key]);
                }
                $return[$key] = $this->_data[$dataType][$key];
            }
            return $return;
        }
        elseif(is_string($keys))
        {
            if (isset ($this->_data[$dataType][$keys]))
            {
                if (!mb_detect_encoding($this->_data[$dataType][$keys],'UTF-8',true)) {
                    return utf8_encode($this->_data[$dataType][$keys]);
                }

                return $this->_data[$dataType][$keys];
            }
        }
        return null;

    }

    public function __toArray($fields=array()){
        $rawData = (array)$this->_data['dirty'];
        $castArray = isset($this->fieldCast)?$this->fieldCast:false;
        $doFilter = count($fields);
        $data = array();
        foreach($rawData as $key=>$value) {
            if ($doFilter && !in_array($key, $fields)) {
                continue 1;
            }
            if ($castArray && isset ($castArray[$key])) {
                switch ($castArray[$key]) {
                    case 'int':
                        $value = (int)$value;
                        break;
                    case 'bool':
                        $value = (bool)$value;
                        break;
                    case 'date':
                        $date = new \DateTime($value);
                        $value = $date->format('c');
                        break;
                    case 'array':
                        $value = unserialize($value);
                        break;
                    case 'json':
                        $value = json_decode($value,true);
                        break;
                    default:
                        break;
                }
            }
            if (is_string($value) && !mb_detect_encoding($value,'UTF-8',true)) {
                $value = utf8_encode($value);
            }
            if ($value !== '') {
                $data[$key] = $value;
            }
        }
        return $data;
    }
    public function setData(array $data)
    {
        $this->_data['dirty'] = $data;
    }
    public function getData($onlyDirty=false)
    {
        if ($onlyDirty===true)
        {
            return  $this->get($this->_dirtyKeys,'dirty');
        }
        if ($onlyDirty=='orginal')
        {
            return $this->_data['orginal'];
        }
        return $this->_data['dirty'];
    }
    public function isDirty()
    {
        return count($this->_dirtyKeys);
    }
    protected static $memcached = null;
    public static function setMemcached($mem) {
        self::$memcached = $mem;
    }
}
