<?php
namespace Carpathia\Mapper;
use Aura\Signal\Manager as SignalManager;

abstract class AMapper {
    protected $_registry = array(
        'dirty'   => [],
        'old'     => [],
        'new'     => [],
        'objects' => []
    );

    protected static $_dbHandler = null;
    
    protected static $memcached = null;

    protected $_cache = null;

    protected $rowsetClass = '\\Carpathia\\Mapper\\Rowset';
    protected $table = '';
    protected $relations = array();
    protected $order = 'date DESC';
    protected $idColumn = 'id';

    protected $_info = array();

    protected $config = array();

    /**
     * Detta är våran Identity Map, den innehåller
     * alla objekt som har en motsvarande rad i databasen
     * varesig dem finns i $_dirtyObjects eller $_oldObjects
     *
     * @var mixed
     */
    protected $_persistedObjects = array();

    protected $signal = null;

    public function getTable() {
        return $this->table;
    }
    public function getTableName()
    {
        return $this->_info['table'];
    }
    public function getDbName()
    {
        return $this->_info['db'];
    }
    protected function setup() {}
    protected function __construct($signal)
    {
        $this->signal = $signal;
        $dbandtbl = \explode('.', $this->table);
        if(\count($dbandtbl) > 1)
        {
            list($db,$tbl) = $dbandtbl;
        }
        else
        {
            $tbl = $dbandtbl[0];
            $db = '';
        }
        $this->_info['db'] = $db;
        $this->_info['table'] = $tbl;
        $this->setup();
    }

    public static function setMemcached($mem){
        self::$memcached = $mem;
    }
    public static function setDbHandler( \Carpathia\Db\IConnection $handler )
    {
        self::$_dbHandler = $handler;
    }
    public static function getDbHandler() {
        return self::$_dbHandler;
    }
    public function rawExecute($sql,$params=array()) {
        return $this->_execute($sql, $params);
    }
    private static $stmtStore = array();
    private static $useStmtStore = true;
    public static function disableStmtStore() {
        self::$useStmtStore = false;
    }
    public static function clearStmtStore() {
        self::$stmtStore = [];
    }
    protected function _execute( $sql, $params=array( ), array $options=array( ) )
    {
        $options['mapper'] = $this;
        $stmtKey = md5(serialize(array($sql,$options)));
        if (self::$useStmtStore && isset(self::$stmtStore[$stmtKey])) {
            $stmt = self::$stmtStore[$stmtKey];
        } else {
            self::$stmtStore[$stmtKey] = $stmt = self::$_dbHandler->prepare ( $sql, $options );
        }
        $rslt = $stmt->execute ( $params );
        
        if ( $rslt )
        {
            if (isset($options['force_stmt'])) {
                return $stmt;
            }
            switch ( substr ( strtolower($sql), 0, 6 ) )
            {
                case 'delete':
                case 'update':
                    return $stmt->rowCount();
                    break;
                case 'insert':
                    return $stmt->lastInsertId();
                    break;
                default :
                    return $stmt;
                    break;
            }
        } else {
            $this->errorStmt = $stmt;
        }
        return false;
    }

    public function register( AObject $obj, $type )
    {
        $type = \strtolower ($type);
        if( !\in_array ( $type, array('new', 'old', 'dirty') ) )
        {
            return false;
        }

        $this->_registry[$type] = $obj;
        return true;
    }
    public function findById( $id ) {
        $where = array(array($this->idColumn.'=?',$id));
        return $this->findOne($where);
    }
    public function getLastId(array $where = array()) {
        list($where,$params) = $this->_buildWhere($where);

        $sql = 'SELECT '.$this->idColumn.' FROM '.$this->table.' WHERE '.$where.' ORDER BY '.$this->idColumn.' DESC LIMIT 1';
        $stmt = $this->_execute($sql,$params);
        $row = $stmt->fetch();

        return $row->id;
    }
    public function findOne( array $where ,$order=null)
    {
        list($where,$params) = $this->_buildWhere($where);
        if (is_string($order)) {
            $order = ' ORDER BY '.$order;
        } else {
            $order = '';
        }
        $sql = 'SELECT * FROM '.$this->table.' WHERE '.$where.$order.' LIMIT 1';
        $options = array('cache_key'=>$this->entry($id), 'life'=>0);
        return $this->findBySql($sql, $params, $options);
    }

    public function fetchColumn($column,$where) {
        list($where,$params) = $this->_buildWhere($where);
        $sql = 'SELECT '.$column.' FROM '.$this->table.' WHERE '.$where;
        $stmt = $this->_execute($sql,$params);
        $r = array();
        foreach($stmt->fetchAll() as $row) {
            $r[] = $row->$column;
        }
        return $r;
    }
    public function countAll($where=array())
    {
        list($where,$params) = $this->_buildWhere($where);
        $sql = 'SELECT COUNT('.$this->idColumn.') as nr FROM '.$this->table.' WHERE '.$where;
        $stmt = $this->_execute($sql,$params);
        $count = $stmt->fetchColumn(0);
        return $count;
    }
    public function findAll($where=array(), $order=null, $offset=0, $limit=10)
    {
        if ($order!==false) {
            $order = ($order===null?$this->order:$order);
        }
        list($where,$params) = $this->_buildWhere($where);
        $sql = 'SELECT * FROM '.$this->table.' WHERE %s %s LIMIT %d,%d';
        $sql = sprintf($sql, $where, $order?'ORDER BY '.$order:'', $offset, $limit);
        $options = array('rowset'=>true);
        return $this->findBySql($sql, $params, $options);
    }

    protected function _getMapperObjectName()
    {
        return '\\Carpathia\\Mapper\\Object';
    }

    /**
     *
     * @param stdObject $data
     * @param mixed $key
     * @return Object
     */
    public function loadObject( $data, $key = false )
    {
        if ( $key === false )
        {
            $key = $data->id;
        }
        if (isset ($this->_persistedObjects[$key]) )
        {
            return $this->_persistedObjects[$key];
        }

        $objName = $this->_getMapperObjectName();
        $obj = new $objName ( $data, array('mapper' => $this->getMapperName()) );
        $this->_persistedObjects[$key] = $obj;
        return $obj;
    }

    public function entry($key='')
    {
        return $key;
        //J-Mapper-Role-v1-134-v2
        $keyBase = sprintf('Carpathia/Mapper/%s/',$this->getMapperName(true));

        $mapperVersionKey = $keyBase.'version';
        $mapperVersion = (int)$this->_cache->fetch($mapperVersionKey);

        $dataVersionKey = sprintf($keyBase.'%d/%s/version',
                                        $mapperVersion, $key);
        $dataVersion = (int)$this->_cache->fetch($dataVersionKey);

        $dataKey = sprintf($keyBase.'%d/%s/%d/data',
                                $mapperVersion, $key, $dataVersion);
        return $dataKey;
    }
    public function updateVersion($key=null)
    {
        $keyBase = sprintf('Carpathia/Mapper/%s/',$this->getMapperName(true));

        $mapperVersionKey = $keyBase.'version';
        if($key === null)
        {
            return $this->_cache->increment($mapperVersionKey);
        }
        else
        {
            $mapperVersion = (int)$this->_cache->fetch($mapperVersionKey);
            $dataVersionKey = sprintf($keyBase.'%d/%s/version',
                                        $mapperVersion, $key);
            return $this->_cache->increment($dataVersionKey);
        }
    }

    protected function _fetchData($sql, $params, $options=array())
    {
        $cacheKey = $options['cache_key'];

        if (!($data = false/*$this->_cache->fetch($cacheKey)*/))
        {
            $stmt = $this->_execute($sql, $params);
            if ($stmt !== false && $stmt !== null)
            {
                $stmt->setFetchMode(\PDO::FETCH_OBJ);

                $data = $stmt->fetchAll();
            }
            else
            {
                return false;
            }
        }
        $params = array('data'=>$data);
        return $params['data'];
    }
    /**
     *
     * @param string | \yoLoo\Sql $sql
     * @param array $params
     * @param array $options
     * @return Rowset | Object
     */
    public function findBySql( $sql, array $params = array( ),
            $options = array( ) )
    {
        if(!isset ($options['cache_key']))
        {
            $options['cache_key'] = $this->entry(md5($sql. serialize($params)));
        }

        if(isset ($options['life']))
        {
            //$this->_cache->setLife($options['life']);
        }

        $data = $this->_fetchData($sql, $params, $options);

        $forceRowset = (bool)(isset($options['rowset'])?$options['rowset']:false);

        if(count($data)==1 && !$forceRowset)
        {
            return $this->loadObject($data[0]);
        }
        if(count($data)==0)
        {
            return null;
        }
        $config = \array_merge((array)$this->config['rowset_config'],(array)(isset($options['rowset_config'])?$options['rowset_config']:array()));
        return $this->createRowset($data, $config);
    }
    public function createRowset($data, $config=array())
    {
        $rowset = $this->rowsetClass;
        $config['mapper'] = $this;
        return new $rowset($data, $config);
    }
    public function getRelation($rel, $obj,$extra=array()){
        if (!isset($this->relations[$rel])) return false;

        $relation = $this->relations[$rel];
        $mapper = $relation['mapper'];
        $data = null;
        if (is_string($mapper)) {
            $mapper = $this->load($mapper,isset($relation['cache'])?$relation['cache']:false);
        }
        $mapper_name = $mapper->getMapperName();
        $where = isset($extra['where'])?$extra['where']:array();
        $order = isset($extra['order'])?$extra['order']:null;
        $offset = isset($extra['offset'])?$extra['offset']:0;
        $limit = isset($extra['limit'])?$extra['limit']:15;

        if (isset($extra['method']) && $extra['method'] == 'count') {
            $count = true;
        }
        if (isset($relation['foreign_key'])) {
            $key = $relation['foreign_key'];
            if (isset($obj->$key)) {
                if (isset($relation['method']))
                {
                    $method = $relation['method'];
                    $where = $obj->get($key);
                } else {
                    $method = 'findAll';
                    if ($count) {
                        $method = 'countAll';
                    }
                    $where[] = array($key.'=?',$obj->get($key));
                    if (isset($relation['type']) && $relation['type']==='one') {
                        $method = 'findOne';
                    }
                }
                if (method_exists($mapper_name, $method)) {
                    $data = $mapper->$method($where,$order, $offset, $limit);
                }
            }
        } else {
            $class = strtolower(substr(get_class($obj),strrpos(get_class($obj),'\\')+1));

            if (isset($relation['method'])) {
                $method = $relation['method'];
            } else {
                $method = 'findBy'.ucfirst($class);
                if ($count) {
                    $method = 'countBy'.ucfirst($class);
                }
            }
            if (method_exists($mapper_name, $method)) {
                $data = $mapper->$method($obj,$where, $order, $offset, $limit);
            } else {
                $method = 'findAll';
                if ($count) {
                    $method = 'countAll';
                }
                $where[] = array($class.'_id = ?',$obj->getId());
                if (isset($relation['type']) && $relation['type']==='one') {
                    $method = 'findOne';
                }
                $data = $mapper->$method($where,$order, $offset,$limit);
            }
        }
        $params = array(
            'relation'=>$rel,
            'data'=>$data
        );
        $data = $params['data'];
        if ($count){
            return ($data instanceof AObject)?1:(int)$data;
        }
        if (($data instanceof Rowset) || ($data instanceof AObject) || is_array($data))
        {
            return $data;
        }
        return null;

    }

    /**
     * Sparar, uppdaterar och tar bort alla objekt som ändrats på något sätt
     *
     * @see Base::update()
     * @see Base::insert()
     * @see Base::delete()
     */
    public function commit()
    {
        foreach ( $this->_registry['new'] as $object )
        {
            $this->insert ( $object );
        }
        foreach ( $this->_registry['dirty'] as $object )
        {
            $this->update ( $object );
        }
        foreach ( $this->_registry['old'] as $object )
        {
            $this->delete ( $object );
        }
    }
    protected function _registryMove($obj, $from, $to)
    {
        if ($from=='new' && $to == 'persisted')
        {
            $this->_persistedObjects[$obj->getId()] = $obj;
        }
    }

    /**
     *
     * @param Object $obj
     */
    public function insert( $obj )
    {
        $sql = 'INSERT INTO %s (%s) VALUES(%s)';
        if ($obj instanceof AObject)
        {
            $inData = $obj->getData();
        }
        elseif (is_array($obj) )
        {
            $inData = $obj;
        }

        foreach ($inData as $key=>$value)
        {
            if (is_array($value))
            {
                if (isset($value['type']) && $value['type']=='expr')
                {
                    $keys[] = $key;
                    $values[] = $value['value'];
                }
                else
                {
                    $keys[] = '`'.$key.'`';
                    $values[] = '? ';
                    $data[] = $value['value'];
                }
            }
            else
            {
                $keys[] = '`'.$key.'`';
                $values[] = '? ';
                $data[] = $value;
            }
        }
        $sql = sprintf($sql,$this->table, implode(', ', $keys), implode(', ', $values));
        $id = $this->_execute($sql, $data);
        if ($id && $obj instanceof AObject)
        {
            $obj->setId($id);
            $this->_registryMove($obj, 'new', 'persisted');
        }
        return $id;
    }
    /**
     *
     * @param Object $obj
     */
    public function delete( $obj ) {

    }

    public function updateMultiple($updateData, $where) {

        list($where,$params) = $this->_buildWhere($where);
        $sql = 'UPDATE %s SET %s WHERE %s';
        $inData = $updateData;
        foreach ($inData as $key=>$value)
        {
            if (is_array($value)) {
                if (isset($value['type']) && $value['type']=='expr') {
                    $cols .= '`'.$key.'`='.$value['value'].',';
                } else {
                    $cols .= '`'.$key.'`=?, ';
                    $data[] = $value['value'];
                }
            } else {
                $cols .= '`'.$key.'`=?, ';
                $data[] = $value;
            }
        }
        $sql = sprintf($sql,$this->table, trim($cols, ', '),$where);

        $rs = $this->_execute($sql, array_merge($data,$params));
        return $rs;
    }

    /**
     *
     * @param Object $obj
     */
    public function update($obj, $key=null)
    {
        $sql = 'UPDATE %s SET %s WHERE '.$this->idColumn.'=? LIMIT 1';
        if ($obj instanceof AObject)
        {
            $inData = $obj->getData(true);
        }
        elseif (is_array($obj) && $key!==null)
        {
            $inData = $obj;
        }
        foreach ($inData as $key=>$value)
        {
            if (is_array($value)) {
                if (isset($value['type']) && $value['type']=='expr') {
                    $cols .= '`'.$key.'`='.$value['value'].',';
                } else {
                    $cols .= '`'.$key.'`=?, ';
                    $data[] = $value['value'];
                }
            } else {
                $cols .= '`'.$key.'`=?, ';
                $data[] = $value;
            }
        }
        $sql = sprintf($sql,$this->table, trim($cols, ', '));
        if ($obj instanceof AObject)
        {
            $data[] = $obj->getId();
        }
        elseif (is_array($obj) && $key !== null)
        {
            $data[] = $key;
        }
        $rs = $this->_execute($sql, $data);
        return $rs;
    }

    /**
     *
     * @return Object
     */
    public function fetchNewRow()
    {
        $rowName = $this->_getMapperObjectName();
        $row = new $rowName ( array(), array('mapper'=>$this->getMapperName()) );

        return  $row;
    }

    /**
     *
     * @param String $mapper
     * @return Base
     */
    public function load( $mapper = null ,$cache = false)
    {
        return self::loadMapper( $mapper ,$cache);
    }

    /**
     *
     * @var Base[]
     */
    protected static $_mappers = array( );

    /**
     *
     * @param <type> $onlyClassName
     * @return <type>
     */
    function getMapperName($onlyClassName=false)
    {
        if ($onlyClassName)
        {
            $name = array_pop(explode('\\', get_class($this)));
            if ($name !== null)
            {
                return $name;
            }
        }
        return get_class($this);
    }

    private static $_signal;

    /**
     *
     * @param String $mapper
     * @return Base
     */
    public static function loadMapper( $mapper = null ,$doCache = false)
    {
        if ( $mapper === null )
        {
            $mapper = get_called_class();
        }
        if ( isset ( self::$_mappers[$mapper] ) )
        {
            if ($doCache && (self::$_mappers[$mapper] instanceof AMapper)) {
                $cls = self::$_mappers[$mapper];
            } else {
                return self::$_mappers[$mapper];
            }
        } else {
            $cls = new $mapper(self::$_signal);
        }
        if ($doCache && self::$_cacheFactory) {
            $cls = self::$_cacheFactory->newInstance($cls);
        }
        return self::$_mappers[$mapper] = $cls;
    }
    private static $_cacheFactory = false;
    public static function setCacheFactory($factory) {
        self::$_cacheFactory = $factory;
    }

    public static function setSignalManager(SignalManager $signal){
        self::$_signal = $signal;
    }

    public function createWhere($where) {
        return $this->_buildWhere($where);
    }

    protected function _buildWhere($where)
    {
        if (count($where)==0)
        {
            return array(' 1=1 ', array());
        }
        $sql = '';
        $params = array();
        foreach($where as $k){
            if (is_array($k)){
                list($q, $v) = $k;
                if (stripos($q, ' in ') || stripos($q, ' in(')) {
                    if (is_array($v)) {
                        $iv = [];
                        foreach ($v as $value) {
                            if (is_string($value)) {
                                $iv[] = "'".$value."'";
                            } else {
                                $iv[] = $value;
                            }
                        }
                        $q = str_replace('?', implode(',',$iv),$q);
                        $sql .= ' '.$q .' AND';
                    } else {
                        $sql .= ' '.$q .' AND';
                        $params[] = $v;
                    }
                } else {
                    $sql .= ' '.$q .' AND';
                    if (is_array($v)) {
                        foreach ($v as $value) {
                            $params[] = $value;
                        }
                    } else {
                        $params[] = $v;
                    }
                }

            }
            else
            {
                $sql .= ' '.$k.' AND';
            }
        }
        if (substr($sql, -4)==' AND')
        {
            $sql = substr($sql, 0, -4);
        }
        return array($sql, $params);
    }
}
