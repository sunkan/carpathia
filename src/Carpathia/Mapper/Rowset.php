<?php
namespace Carpathia\Mapper;

use \Carpathia\Collection;

class Rowset extends Collection\ACollection implements IRowset
{
    public function __sleep() {
        if (($this->_mapper instanceof AMapper)) {
            $this->_mapper = $this->_mapper->getMapperName();
        }
        return array('_mapper','_config','_data','_count','_pointer');
    }
    public function __toArray() {
        $data = [];
        foreach ($this as $row) {
            $data[] = $row->__toArray();
        }
        return $data;
    }
    public function __wakeup() {

    }
    public function merge(Rowset $rowset) {
        $data = array_merge($this->_data, $rowset->_data);
        $config = array_merge($this->_config, $rowset->_config);

        if (($this->_mapper instanceof AMapper)) {
            $config['mapper'] = $this->_mapper->getMapperName();
        } else {
            $config['mapper'] = $this->_mapper;
        }
        return new Rowset($data, $config);
    }

    public function __construct($data, $config)
    {
        parent::__construct($data);
        $this->_config = $config;
        try {
            $this->_pager = new \Celestis\Pager\Blog();
        } catch(\Aura\Autoload\Exception\NotFound $e) {

        }
        $this->_mapper = $this->_config['mapper'];
        unset($this->_config['mapper']);
    }
    private $keys = array();
    protected $_mapper = null;
    public function getById($id) {
        foreach ($this->_data as $index=>$data) {
            if ($data->id==$id) {
                return $this->loadIndex($index);
            }
        }
        return false;
    }
    public function getKeys($key){
        if (count($this->keys)) return $this->keys;

        foreach($this->_data as $data){
            $this->keys[] = $data->$key;
        }
        return $this->keys;
    }
    public function loadIndex($index, $conf=array())
    {
        $conf = \array_merge($this->_config, $conf);
        if (empty($this->_rows[$index])) {
            if (is_array($conf['keys']))
            {
                $key = '';
                foreach ($conf['keys'] as $keys) {
                    $key .= md5($this->_data[$index]->$keys);
                }
                $key = md5($key);
            } else {
                $key = $this->_data[$index]->id;
            }

            return $this->getMapper()->loadObject($this->_data[$index], $key);
        }
        return $this->_rows[$index];
    }
    public function isLast() {
        return (count($this->_data)-1)==$this->_pointer;
    }
    public function getRow($pos)
    {
        $key = $this->key();
        try
        {
            $this->seek($position);
            $row = $this->current();
        }
        catch (\OutOfBoundsExceptions $e)
        {
            throw new Exception('No row could be found at position ' . (int) $position);
        }
        $this->seek($key);

        return $row;
    }

    public function setMapper(AMapper $mapper)
    {
        $this->_mapper = $mapper;
    }
    public function getMapper()
    {
        if (!($this->_mapper instanceof AMapper)) {
            $this->_mapper = $this->_loadMapper();
        }
        return $this->_mapper;
    }
    protected function _loadMapper($name=false)
    {
        if ($name===false)
        {
            $name = $this->_mapper;
        }
        if (is_string($name))
        {
             return AMapper::loadMapper($name);
        }
        return $name;
    }

    public function delete()
    {
        foreach ($this as $row)
            $row->delete();
    }
    public function save()
    {
        foreach ($this as $row)
            $row->save();
    }

    protected $_pager = null;
    public function getPager()
    {
        return $this->_pager;
    }
    public function setPager(\Carpathia\Pager\IPager $pager)
    {
        $this->_pager = $pager;

        return $this;
    }

    public function setPagerInfo(array $data)
    {
        $keys = array('pages', 'current');
        foreach ($keys as $key)
        {
            if (!isset($data[$key]))
            {
                if ($key == 'current')
                {
                    $data['current'] = $this->_mapper->getPage();
                }
                if ($key == 'pages')
                {
                    $data['pages'] = $this->_mapper->countAll();
                }
            }
        }
        $this->_pager->setInfo($data);
    }
    public function getPagerInfo()
    {
        return $this->_pager->getInfo();
    }
}
